#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<string.h>
#include<ctype.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<sys/wait.h>
#include<signal.h>
#include<errno.h>
#include<pthread.h>

#define SERV_PORT 38383
#define SERV_IP "121.36.24.26"
#define NUM 3

int std_err(const char* name)
{
    perror(name);
    exit(1);
}

int main(void)
{
    int cfd, ret;
    char buf[BUFSIZ];
    pid_t pid;

    int i;
    for(i = 0; i < NUM; i++){
        pid = fork();
        if(pid == 0)
            break;
        else if(pid < 0)
            std_err("fork");
    }

    //子进程逻辑
    if(pid == 0)
    {
        //创建套节字
        cfd = socket(AF_INET, SOCK_STREAM, 0);
        if(cfd == -1)
            std_err("socket");

        //定义IP ， 端口
        struct sockaddr_in clie_addr;
        clie_addr.sin_family = AF_INET;
        clie_addr.sin_port = htons(SERV_PORT);

        //转换IP 字符串的地址
        ret = inet_pton(AF_INET, SERV_IP, &clie_addr.sin_addr.s_addr);
        if(ret != 1)
            std_err("inet_pton");

        //链接服务器
        ret = connect(cfd, (struct sockaddr*)&clie_addr, sizeof(clie_addr));
        if(ret == -1)
            std_err("connect");

    char buff[256];
    int nRecv=recv(cfd,buff,256,0);
    if(nRecv>0)
    {   
        buff[nRecv]='\0';
        printf("当前时间：%s\n",buff);
    } 
    }
    //关闭套节字
    close(cfd);
    return 0;
}